﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Beadando
{
        /// <summary>
        /// Bindable osztály- támogatja a Boindingekhez szükséges OnPropertyChanged metódust
        /// </summary>
        public class Bindable : INotifyPropertyChanged
        {
            /// <summary>
            /// propertychanged events 
            /// </summary>
            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// A bindeléshez a metódus
            /// </summary>
            /// <param name="propertyname">callermembername-mel automatikus paraméter</param>
            protected void OnpropertyChanged([CallerMemberName] string propertyname = null)
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
                }
            }
       }
}
