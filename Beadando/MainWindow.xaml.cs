﻿using Beadando.Ablakok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// A menü modellje
        /// </summary>
        private MenuViewModel menuviewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class
        /// Default constructor
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.menuviewModel = new MenuViewModel();
            this.DataContext = this.menuviewModel;
        }

        /// <summary>
        /// A játék gomb klikk eventjére feliratkoztatott metódus-elindítja a játékot
        /// </summary>
        /// <param name="sender">sender a</param>
        /// <param name="e">e a</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            this.menuviewModel.Jatek();
            this.Show();
        }

        /// <summary>
        /// A highscores gomb eventjének metódusa
        /// </summary>
        /// <param name="sender">A küldő objektum</param>
        /// <param name="e">Az e</param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Hide();
            this.menuviewModel.HighScoresAblak();
            this.Show();
        }

        /// <summary>
        /// A bezáró gomb metódusa- bezárja az ablakot
        /// </summary>
        /// <param name="sender">a sender</param>
        /// <param name="e">e a</param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }        
    }
}
