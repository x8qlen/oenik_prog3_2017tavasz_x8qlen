﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beadando
{
    class Eredmeny:IComparable
    {
        private int score;

        public int Score
        {
            get { return score; }
            
        }
        private string name;

        public string Name
        {
            get { return name; }
            
        }
        public Eredmeny(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
        public override string ToString()
        {
            return score + " " + Name;
        }




        public int CompareTo(object obj)
        {
            Eredmeny masik=obj as Eredmeny;

            if (masik.Score < this.Score)
            {
                return -1;
            }
            else if (masik.Score > this.Score)
            {
                return 1;
            }
            else return 0;
        }
    }
}
