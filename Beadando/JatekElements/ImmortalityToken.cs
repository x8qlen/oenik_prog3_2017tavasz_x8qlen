﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Beadando.JatekElements
{
    /// <summary>
    /// Az erősítésnek az osztálya-játékobjektum leszármazott az ütközések és a megjelenési forma miatt
    /// </summary>
    public class ImmortalityToken : JatekObjektum
    {   
        /// <summary>
        /// Initializes a new instance of the <see cref="ImmortalityToken" /> class.
        /// </summary>
        /// <param name="kozeppont">A token középpontja</param>
        public ImmortalityToken(Point kozeppont)
        {
            Rect rect = new Rect(kozeppont.X, kozeppont.Y, 20, 45);
            this.Geometria = PathGeometry.CreateFromGeometry(new RectangleGeometry(rect));
            this.Megjelenes = new Path();
            this.Megjelenes.Data = this.Geometria;
            this.Megjelenes.Fill = Brushes.Green;
        }
    }
}
