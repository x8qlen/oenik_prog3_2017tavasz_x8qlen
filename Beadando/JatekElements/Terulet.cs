﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando.JatekElements
{
    /// <summary>
    /// Terület osztály- segít az akadályok helyének vizsgálatában- mindig négyszög, Játékobjektum leszármazott(ütközés metódus miatt)
    /// </summary>
    public class Terulet : JatekObjektum
    {
        /// <summary>
        /// A terület bal felső sarka - a terület megadásához, koordináták vizsgálatához
        /// </summary>
        private Point balfelso;

        /// <summary>
        /// A terület jobb alsó sarka - a terület megadásához, koordináták vizsgálatához
        /// </summary>
        private Point jobbalso;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Terulet" /> class.
        /// </summary>
        /// <param name="balfelsosarok">a bal felső sarka a területnek</param>
        /// <param name="jobbalsosarok">a jobb alsó sarka a területnek</param>
        public Terulet(Point balfelsosarok, Point jobbalsosarok)
        {
            Rect rect = new Rect(balfelsosarok, jobbalsosarok);
            this.Geometria = PathGeometry.CreateFromGeometry(new RectangleGeometry(rect));
            this.Balfelso = balfelsosarok;
            this.jobbalso = jobbalsosarok;
        }
        
        /// <summary>
        /// Gets or sets the value of balfelso
        /// </summary>
        public Point Balfelso
        {
            get { return this.balfelso; }
            set { this.balfelso = value; }
        }

        /// <summary>
        /// Gets or sets the value of jobbalso
        /// </summary>
        public Point Jobbalso
        {
            get { return this.jobbalso; }
            set { this.jobbalso = value; }
        }
    }
}
