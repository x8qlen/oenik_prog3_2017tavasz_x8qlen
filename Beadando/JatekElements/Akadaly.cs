﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Beadando.JatekElements
{
    /// <summary>
    /// Az akadályok osztálya
    /// </summary>
    public class Akadaly : JatekObjektum
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Akadaly" /> class.
        /// </summary>
        /// <param name="kozeppont">Az akadály középpontja</param>
        public Akadaly(Point kozeppont)
        {
            Rect rect = new Rect(kozeppont.X, kozeppont.Y, 20, 45);
            this.Geometria = PathGeometry.CreateFromGeometry(new RectangleGeometry(rect));
            this.Megjelenes = new Path();
            this.Megjelenes.Data = this.Geometria;
            this.Megjelenes.Fill = Brushes.Blue;
        }
    }
}
