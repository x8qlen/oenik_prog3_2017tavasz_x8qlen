﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using Beadando.ViewModels;
using System.Windows;

namespace Beadando.JatekElements
{
    /// <summary>
    /// A játékobjektumok közös őse, tartalmazza a minden objektumra jellemző tulajdonságoka/metódusokat
    /// </summary>
    public class JatekObjektum : Bindable
    {
        /// <summary>
        /// A megjelnítéshez szükséges shape leszármazottat tartalmazza
        /// </summary>
        private Path megjelenes;

        /// <summary>
        /// Az egyszerűbb ütközéskezelés érdekében létrehozott geometriája a játékobjektumnak
        /// </summary>
        private Geometry geometria;

        /// <summary>
        /// Gets or sets the value of geometria
        /// </summary>
        public Geometry Geometria
        {
            get 
            { 
                return this.geometria;
            } 
            
            set
            {
                this.geometria = value;
                this.OnpropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the value of megjelenes
        /// </summary>
        public Path Megjelenes
        {
            get 
            {
                return this.megjelenes;
            }
            
            set
            {
                this.megjelenes = value;
                this.OnpropertyChanged();
            }
        }
        
        /// <summary>
        /// Vizsgálja a gazdapéldány és a megadott másik objektum ütközését
        /// </summary>
        /// <param name="masik">a másik objektum amivel való ütközést vizsgáljuk</param>
        /// <returns>Visszatérési értéke az, hogy ütközik-e a két objektum</returns>
        public bool Ütközik(JatekObjektum masik)
        {
            Geometry metszet = Geometry.Combine(this.Geometria, masik.Geometria, GeometryCombineMode.Intersect, null); 
            return metszet.GetArea() > 0;
        }
        
        /// <summary>
        /// Transzformálja a gazdapéldányt
        /// </summary>
        /// <param name="transform">az átadott transzformáció amit a példányon végrehajtunk</param>
        public virtual void Transzformal(Transform transform)
        {
            this.Geometria.Transform = transform;
            this.Geometria = this.Geometria.GetFlattenedPathGeometry();
            this.Megjelenes.Data = this.Geometria;
        }
        
        /// <summary>
        /// A mozgáshoz egy könnyebben meghívható metódus
        /// </summary>
        /// <param name="mozgasvektor">a mozgás irányát (és ezzel nagyságát) tartalmazó vektor</param>
        public void Mozog(Vector mozgasvektor)
        {
            this.Transzformal(new TranslateTransform(mozgasvektor.X, 0));
        }
    }
}
