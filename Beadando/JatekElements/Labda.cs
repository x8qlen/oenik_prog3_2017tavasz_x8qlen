﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Beadando.JatekElements
{
    /// <summary>
    /// Labda osztály- a labdát definiálja
    /// </summary>
    public class Labda : JatekObjektum
    {
        /// <summary>
        /// Az aktuális erősítést tároló mező
        /// </summary>
        private string erosites;

        /// <summary>
        /// Az aktuális mozgásirányt tároló mező
        /// </summary>
        private Vector mozgasirany;

        /// <summary>
        /// Az aktuális középpontot tároló mező(labda kp-ja)
        /// </summary>
        private Point kozeppont;

        /// <summary>
        /// Initializes a new instance of the <see cref="Labda" /> class.
        /// Beállítja az ősosztály megfelelő mezőit.
        /// </summary>
        /// <param name="kezdopont">A labda kezdő(közép)pontja</param>
        public Labda(Point kezdopont) 
        {
            PathGeometry patgeo = PathGeometry.CreateFromGeometry(new EllipseGeometry(kezdopont, 10, 10));
            this.kozeppont = kezdopont;
            this.Geometria = patgeo;
            Path pat = new Path();
            pat.Fill = Brushes.Red;
            pat.Data = patgeo;
            this.Megjelenes = pat;
        }

        /// <summary>
        /// Gets or sets the value of erosites
        /// </summary>
        public string Erosites
        {
            get { return this.erosites; }
            set { this.erosites = value; }
        }

        /// <summary>
        /// Gets or sets the value of mozgasirany
        /// </summary>
        public Vector Mozgasirany
        {
            get { return this.mozgasirany; }
            set { this.mozgasirany = value; }
        }

        /// <summary>
        /// Gets or sets the value of kozeppont
        /// </summary>
        public Point Kozeppont
        {
            get
            {
                return this.kozeppont;
            }

            set
            {
                this.kozeppont = value;
                this.OnpropertyChanged();
            }
        }

        /// <summary>
        /// A labda mozgási metódusa- azért kell neki külön, mert a középpontját 
        /// is változtatni kell vele- ugyanis ez csak egy mezőnév, nem része az ősosztálynak.
        /// </summary>
        /// <param name="canvas">A canvas amiben a mozgást végzi a labda</param>
        public void Mozgas(Canvas canvas)
        {
                this.Transzformal(new TranslateTransform(0, this.Mozgasirany.Y));
                this.Kozeppont = new Point(this.Kozeppont.X + this.Mozgasirany.X, this.Kozeppont.Y + this.Mozgasirany.Y);
            if (this.kozeppont.X < 0 || this.kozeppont.X > canvas.ActualWidth)
            {
                this.mozgasirany.X = -this.mozgasirany.X;
            }
            else if (this.kozeppont.Y < 0 || this.kozeppont.Y > canvas.ActualHeight)
            {
                this.mozgasirany.Y = -this.mozgasirany.Y;
            }
        }

        /// <summary>
        /// Beállítja az aktuális erősítést-ez terv szerint több féle lehetett volna- idő hiányában csak egy lett.
        /// </summary>
        /// <param name="powerUpType">Az átadott erősítés</param>
        public void PowerUp(string powerUpType)
        {
            this.erosites = powerUpType;
        }

        /// <summary>
        /// Az erősítést eltűnteti a labdáról
        /// </summary>
        public void PowerUpGone()
        {
            this.Erosites = "nincs";
        }
    }
}
