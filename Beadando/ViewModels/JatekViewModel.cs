﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading.Tasks;
using Beadando.JatekElements;

namespace Beadando.ViewModels
{
    /// <summary>
    /// A játékot kezelő osztály.
    /// </summary>
    public class JatekViewModel : Bindable
    {
        /// <summary>
        /// A mező amiben a felhalmozott halhatatlanságot tároljuk.
        /// </summary>
        private double halhatatlansagStored;

        /// <summary>
        /// Tárolja, hogy éppen van-e aktív erősítés.
        /// </summary>
        private bool powerUpActive;

        /// <summary>
        /// A játékos aktuális pontszáma.
        /// </summary>
        private double score;

        /// <summary>
        /// Változó pontszám- előrehaladással nő, hátramenéssel csökken-> felfelé lökdösi a valódi pontszámot.
        /// </summary>
        private double scoreabsolute;

        /// <summary>
        /// Terület- segít vizsgálni, hogy merre jár az utoljára letett akadály- ebbe
        /// a területbe tesszük az újakat mindig- ezért van a canvas mellett.
        /// </summary>
        private Terulet akadalySpawnTerulet;

        /// <summary>
        /// a labdán kívül minden játékobjektum mozgásvektora- értéke mindig a labda mozgási irányának X koordinátájának negáltja
        /// így elérve az érzetet, hogy a labdát követi a "kamera", míg az előre halad.
        /// </summary>
        private Vector folyamatosmozgas;

        /// <summary>
        /// A játék objektumait tartalmazó lista.
        /// </summary>
        private List<JatekObjektum> objektumok;

        /// <summary>
        /// Logikai változó tárolja, hogy a játék véget ért-e.
        /// </summary>
        private bool jatekvege;
        
        /// <summary>
        /// Az ablak ide adja át az egér pozícióját.
        /// </summary>
        private Point egerPozicio;

        /// <summary>
        /// A játékban a labda mezője
        /// </summary>
        private Labda laszti;

        /// <summary>
        /// Az a terület amibe érve az akadályokat töröljük a listákból(hogy ne teljen a végtelenségbe a memória)
        /// </summary>
        private Terulet akadalyTisztitoTerulet;

       /// <summary>
       /// Initializes a new instance of the <see cref="JatekViewModel" /> class.
       /// Létrehozz a labdát, és a területeket, nullázza a pontszámot.
       /// </summary>
       /// <param name="canvas">Az átadott canvas</param>
        public JatekViewModel(Canvas canvas)
        {
            this.objektumok = new List<JatekObjektum>();

            this.laszti = new Labda(new Point(150, 100));

            canvas.Children.Add(this.laszti.Megjelenes);
            this.score = 0;
            this.scoreabsolute = 0;
            this.jatekvege = false;
            this.AkadalySpawnTerulet = new Terulet(new Point(canvas.Width, 0), new Point(canvas.Width + 60, canvas.Height));
            this.akadalyTisztitoTerulet = new Terulet(new Point(-40, 0), new Point(-20, canvas.Height));
            this.powerUpActive = false;
            this.folyamatosmozgas = new Vector(-1, 0);
        }

        /// <summary>
        /// Gets the value of egerpozicio.
        /// </summary>
        public Point EgerPozicio
        {
            get { return this.egerPozicio; }
        }

        /// <summary>
        /// Gets or sets a labdát.
        /// </summary>
        public Labda Laszti
        {
            get 
            {
                return this.laszti;
            }
            
            set 
            {
                this.laszti = value; 
            }
        }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public Terulet AkadalyTisztitoTerulet
        {
            get
            {
                return this.akadalyTisztitoTerulet;
            }
            
            set 
            {
                this.akadalyTisztitoTerulet = value;
            }
        }

        /// <summary>
        /// Gets or sets tulajdonságot ami a felhalmozott halhatatlanságot tárolja.
        /// </summary>
        public double HalhatatlansagStored
        {
            get 
            {
                return this.halhatatlansagStored;
            }
            
            set
            {
                this.halhatatlansagStored = value;
                this.OnpropertyChanged(); 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the power up is active or not.
        /// </summary>
        public bool PowerUpActive
        {
            get { return this.powerUpActive; }
            set { this.powerUpActive = value; }
        }

        /// <summary>
        /// Gets or sets a játékos aktuális pontszámát.
        /// </summary>
        public double Score
        {
            get 
            {
                return this.score;
            }
            
            set 
            {
                this.score = value;
                this.OnpropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the value of scoreabsolute.
        /// </summary>
        public double Scoreabsolute
        {
            get
            {
                return this.scoreabsolute;
            }
            
            set
            {
                this.scoreabsolute = value;
                this.OnpropertyChanged(); 
            }
        }

        /// <summary>
        /// Gets or sets the value of akadalyspawnterulet.
        /// </summary>
        public Terulet AkadalySpawnTerulet
        {
            get { return this.akadalySpawnTerulet; }
            set { this.akadalySpawnTerulet = value; }
        }

        /// <summary>
        /// Gets or sets the value of folyamatosmozgás.
        /// </summary>
        public Vector Folyamatosmozgas
        {
            get { return this.folyamatosmozgas; }
            set { this.folyamatosmozgas = value; }
        }

        /// <summary>
        /// Gets or sets the value of objektumok.
        /// </summary>
        public List<JatekObjektum> Objektumok
        {
            get { return this.objektumok; }
            set { this.objektumok = value; }
        }

        /// <summary>
        /// Az ablak Tick eseménye által meghívott metódus, ellenőrzi az ütközéseket,
        /// a játék végét, figyeli a mozgási irányokat
        /// Kezeli az ütközést, az erősítést, hozzáadja és kitörli a megfelelő blokkokat.
        /// </summary>
        /// <param name="canvas">játékot tartalmazó canvas</param>
        /// <returns>visszatérési értek az, hogy a játék véget ért-e</returns>
        public bool Tick(Canvas canvas)
        {
            if (this.laszti.Erosites == "Halhatatlansag" && this.halhatatlansagStored > 0)
            {
                this.halhatatlansagStored -= 0.2;
                if (this.halhatatlansagStored <=0)
                {
                    this.laszti.PowerUpGone();
                }
            }

            if (this.egerPozicio != this.laszti.Kozeppont)
            {
                double sebesseghossz = Math.Sqrt((this.folyamatosmozgas.X * this.folyamatosmozgas.X) + (this.laszti.Mozgasirany.Y * this.laszti.Mozgasirany.Y));
                double ujiranyhossz = Math.Sqrt(((this.egerPozicio.X - this.laszti.Kozeppont.X) * (this.egerPozicio.X - this.laszti.Kozeppont.X)) + ((this.egerPozicio.Y - this.laszti.Kozeppont.Y) * (this.egerPozicio.Y - this.laszti.Kozeppont.Y)));
                double sebessegarany = sebesseghossz / ujiranyhossz;

                this.laszti.Mozgasirany = new Vector(0, (this.egerPozicio.Y - this.laszti.Kozeppont.Y) * sebessegarany);
                this.folyamatosmozgas = new Vector(-(this.egerPozicio.X - this.laszti.Kozeppont.X) * sebessegarany, 0);
            }

            this.laszti.Mozgas(canvas);
            this.Scoreabsolute -= Math.Round(this.folyamatosmozgas.X);
            if (this.Scoreabsolute > this.Score)
            {
                this.Score = this.Scoreabsolute;
            }

            if
                (this.Score % 150 == 0)
            {
                this.laszti.Mozgasirany = new Vector(this.laszti.Mozgasirany.X * 1.4, this.laszti.Mozgasirany.Y * 1.4);
            }

            for (int i = 0; i < this.objektumok.Count; i++)
            {
                JatekObjektum akadaly = this.objektumok[i];
                akadaly.Mozog(this.folyamatosmozgas);
                if (this.laszti.Ütközik(akadaly))
                {
                    if (( this.HalhatatlansagStored==0 || this.laszti.Erosites != "Halhatatlansag") && akadaly is Akadaly)
                    {
                        this.jatekvege = true;
                    }
                    else if (akadaly is ImmortalityToken)
                    {
                        if (this.halhatatlansagStored + 20 > 100)
                        {
                            this.halhatatlansagStored = 100;
                        }
                        else
                        {
                            this.halhatatlansagStored += 20;
                        }

                        canvas.Children.Remove(akadaly.Megjelenes);
                        this.objektumok.Remove(akadaly);
                    }
                }
               
                if (akadaly.Ütközik(this.akadalyTisztitoTerulet))
                {
                    canvas.Children.Remove(akadaly.Megjelenes);
                    this.objektumok.Remove(akadaly);
                }
            }

            this.AkadalySpawn(canvas);
            return this.jatekvege;
        }
        
        /// <summary>
        /// Az ablak hívja meg - ezzel adja át az egér pozícióját.
        /// </summary>
        /// <param name="pozicio">az egér pozíciója</param>
        public void Egermozog(Point pozicio)
        {
            this.egerPozicio = pozicio;
        }

        /// <summary>
        /// Letesz két új akadályt a spawnterületre- és 10% eséllyel egy erősítő blokkot is
        /// </summary>
        /// <param name="canvas">átadott canvas</param>
        private void AkadalySpawn(Canvas canvas)
        {
            bool spawnolhat = true;
            if (this.objektumok.Count > 0)
            {
                foreach (JatekObjektum akadaly in this.objektumok)
                {
                    if (akadaly is Akadaly && akadaly.Ütközik(this.akadalySpawnTerulet))
                    {
                        spawnolhat = false;
                    }
                }
            }

            if (spawnolhat)
            {
                Random rndloc = new Random();
                Random rndboost = new Random();
                for (int i = 0; i < 2; i++)
                {
                    double randx = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.X + 10, (int)this.akadalySpawnTerulet.Jobbalso.X);
                    double randy = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.Y + 23, (int)this.akadalySpawnTerulet.Jobbalso.Y - 23);
                    Akadaly ujakadaly = new Akadaly(new Point(randx, randy));
                    this.objektumok.Add(ujakadaly);
                    canvas.Children.Add(ujakadaly.Megjelenes);
                }

                if (rndboost.Next(0, 10) == 3)
                {
                    double randx = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.X + 10, (int)this.akadalySpawnTerulet.Jobbalso.X);
                    double randy = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.Y + 23, (int)this.akadalySpawnTerulet.Jobbalso.Y - 23);
                    ImmortalityToken ujimmoto = new ImmortalityToken(new Point(randx, randy));
                    while (ujimmoto.Ütközik(this.objektumok[this.objektumok.Count - 1]) || ujimmoto.Ütközik(this.objektumok[this.objektumok.Count - 2]))
                    {
                        randx = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.X + 10, (int)this.akadalySpawnTerulet.Jobbalso.X);
                        randy = rndloc.Next((int)this.akadalySpawnTerulet.Balfelso.Y + 23, (int)this.akadalySpawnTerulet.Jobbalso.Y - 23);
                        ujimmoto = new ImmortalityToken(new Point(randx, randy));
                    }

                    this.objektumok.Add(ujimmoto);
                    canvas.Children.Add(ujimmoto.Megjelenes);
                }
            }
        }
    }
}
