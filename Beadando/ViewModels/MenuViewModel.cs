﻿using Beadando.Ablakok;
using Beadando.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Beadando
{
    /// <summary>
    /// A menü viewmodelje
    /// </summary>
    public class MenuViewModel
    {
        /// <summary>
        /// Az eredmények ablak mezője
        /// </summary>
        private HighScores highscores;

        /// <summary>
        /// A játékablak mezője
        /// </summary>
        private JatekAblak jatekAblak;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel" /> class.
        /// </summary>
        public MenuViewModel()
        {
        }

        /// <summary>
        /// Gets or sets the value of highscores property
        /// </summary>
        public HighScores HighScores
        {
            get { return this.highscores; }
            set { this.highscores = value; }
        }

        /// <summary>
        /// Gets or sets the value of jatekablak
        /// </summary>
        public JatekAblak JatekAblak
        {
            get { return this.jatekAblak; }
            set { this.jatekAblak = value; }
        }
        
        /// <summary>
        /// Meghívja a játékot=ablaknyitás
        /// </summary>
        public void Jatek()
        {
            this.jatekAblak = new JatekAblak();
            if (this.jatekAblak.ShowDialog() == true)
            {
                StreamWriter sw = new StreamWriter("highscores.txt", true);
                sw.WriteLine(this.jatekAblak.FinalScore + " - " + this.jatekAblak.TextBoxName.Text);
                sw.Close();
            }
        }

        /// <summary>
        /// Meghívja az eredmények ablakát
        /// </summary>
        public void HighScoresAblak()
        {
            this.highscores = new HighScores();
            if (this.highscores.ShowDialog() == true)
            {
            }
        }
    }
}
