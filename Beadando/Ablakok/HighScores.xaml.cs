﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Beadando.Ablakok
{
    /// <summary>
    /// Interaction logic for HighScores.xaml
    /// </summary>
    public partial class HighScores : Window
    {
        /// <summary>
        /// Az eredményeket tartalmazó lista
        /// </summary>
        private List<Eredmeny> lista;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScores" /> class.
        /// </summary>
        public HighScores()
        {
            this.InitializeComponent();
            StreamReader sr = new StreamReader("highscores.txt");
            this.lista = new List<Eredmeny>();
            while (!sr.EndOfStream)
            {
                string s = sr.ReadLine();
                int i=0;
                string scorestring="";
                while (s[i]!=' ')
                {
                    scorestring += s[i];
                    i++;
                }
                string namestring="";
                i++;
                while (i<s.Length)
                {
                    namestring += s[i];
                    i++;
                }
                int scorenum = int.Parse(scorestring);
                this.lista.Add(new Eredmeny(namestring, scorenum));
            }

            this.lista.Sort();
            
            

            sr.Close();
            listboxHighScores.ItemsSource = this.lista;
        }

        /// <summary>
        /// Az Ok button metódusa- bezárja az ablakot
        /// </summary>
        /// <param name="sender">A küldő</param>
        /// <param name="e">Az e</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
