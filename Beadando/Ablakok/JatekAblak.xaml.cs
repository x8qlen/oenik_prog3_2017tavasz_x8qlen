﻿using Beadando.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Beadando.Ablakok
{
    /// <summary>
    /// Interaction logic for JatekAblak.xaml
    /// </summary>
    public partial class JatekAblak : Window
    {
        /// <summary>
        /// A modell ami az egész játékot kezeli
        /// </summary>
        private JatekViewModel jatekviewModel;
       
        /// <summary>
        /// a játék timere
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Logikai változó értéke arra utal, hogy vége van-e a játéknak
        /// </summary>
        private bool jatekvege;

        /// <summary>
        /// játék kezdeti instrukciója
        /// </summary>
        private Label instrukcio = new Label();
        
        /// <summary>
        /// A játékos végső pontszáma- ez kerül mentésre
        /// </summary>
        private int finalscore;

        /// <summary>
        /// Indicates whether the game has ended.
        /// </summary>
        private Button jatekvegebutton;

        /// <summary>
        /// A név megadásához használt szövegdoboz
        /// </summary>
        private TextBox textBoxName;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="JatekAblak" /> class.
        /// </summary>
        public JatekAblak()
        {
            this.InitializeComponent();
            this.canvasJatek.Focus();
            this.jatekviewModel = new JatekViewModel(canvasJatek);
            this.DataContext = this.jatekviewModel;
            this.timer = new DispatcherTimer();
            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            this.timer.Tick += this.Timer_Tick;
            this.instrukcio.FontSize = 34;
            this.prBarIm.Minimum = 0;
            this.prBarIm.Maximum = 100;
            this.instrukcio.Content = "Nyomd meg a G betűt a kezdéshez!";
            this.canvasJatek.Children.Add(this.instrukcio);
        }
        
        /// <summary>
        /// Gets or sets the value of textboxname
        /// </summary>
        public TextBox TextBoxName
        {
            get { return this.textBoxName; }
            set { this.textBoxName = value; }
        }

        /// <summary>
        /// Gets or sets the value of finalscore
        /// </summary>
        public int FinalScore
        {
            get { return this.finalscore; }
            set { this.finalscore = value; }
        }

        /// <summary>
        /// A timer tick eseményre feliratkoztatott metódus- meghívja a játékmegfelelő metódusát- ha a játéknak vége megjeleníti a szükséges elemeket, beállítja a végső pontszámot
        /// </summary>
        /// <param name="sender">A sender</param>
        /// <param name="e">a timer</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.prBarIm.Value = this.jatekviewModel.HalhatatlansagStored;
            this.jatekvege = this.jatekviewModel.Tick(this.canvasJatek);
            if (this.jatekvege)
            {
                this.timer.Stop();
                Label jatekvegelabel = new Label();
                jatekvegelabel.Content = "A Játéknak vége, pontszámod:" + this.jatekviewModel.Score;
                jatekvegelabel.FontStyle = FontStyles.Oblique;
                jatekvegelabel.FontSize = 30;
                Thickness margin = new Thickness(50, 200, 0, 0);
                jatekvegelabel.Margin = margin;
                this.jatekvegebutton = new Button();
                this.jatekvegebutton.Content = "Köszönöm, hogy játszhattam! :)";
                this.jatekvegebutton.FontSize = 15;
                this.jatekvegebutton.Width = 200;
                this.jatekvegebutton.Height = 25;
                margin = new Thickness(120, 280, 0, 0);
                this.jatekvegebutton.Margin = margin;
                this.jatekvegebutton.Click += this.Vegbutton_click;
                this.TextBoxName = new TextBox();
                this.TextBoxName.Text = "Ide írd a neved";
                this.textBoxName.Width = 140;
                this.textBoxName.Height = 25;
                this.textBoxName.FontSize = 15;
                margin = new Thickness(120, 250, 0, 0);
                this.textBoxName.Margin = margin;
                this.textBoxName.Background = Brushes.Gray;
                canvasJatek.Children.Add(this.jatekvegebutton);
                canvasJatek.Children.Add(jatekvegelabel);
                canvasJatek.Children.Add(this.textBoxName);
                this.finalscore = (int)Math.Round(this.jatekviewModel.Score);
            }
        }

        /// <summary>
        /// A játék végén megjelenő gomb eseményére feliratkoztatott metódus. bezárja az ablakot true értékkel
        /// </summary>
        /// <param name="sender">a sender</param>
        /// <param name="e">Az e</param>
        private void Vegbutton_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// A billentyű lenyomására feliratkoztatott metódus - meghívja a játék megfelelő 
        /// </summary>
        /// <param name="sender">A sender</param>
        /// <param name="e">A lenyomott billentyű</param>
        private void Pegi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
            else if (this.timer.IsEnabled == false && e.Key == Key.G && !this.jatekvege)
            {
                this.timer.Start();
                this.canvasJatek.Children.Remove(this.instrukcio);
            }
            else if (e.Key == Key.D1 && !this.jatekviewModel.PowerUpActive && this.jatekviewModel.HalhatatlansagStored > 0)
            {
                this.jatekviewModel.Laszti.PowerUp("Halhatatlansag");
                this.jatekviewModel.PowerUpActive = true;
            }
        }

        /// <summary>
        /// Az egér mozgására feliratkoztatott metódus- beállítja a játékviewmodellben az egér helyzetét
        /// </summary>
        /// <param name="sender">A sender</param>
        /// <param name="e">Az egér</param>
        private void CanvasJatek_MouseMove(object sender, MouseEventArgs e)
        {
            this.jatekviewModel.Egermozog(new Point(e.GetPosition(this.canvasJatek).X, e.GetPosition(this.canvasJatek).Y));
        }

        /// <summary>
        /// Billentyű felengedésre feliratkoztatott metódus- az erősítést állítja le.
        /// </summary>
        /// <param name="sender">A sender</param>
        /// <param name="e">A felengedett billentyű</param>
        private void CanvasJatek_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D1 && this.jatekviewModel.PowerUpActive)
            {
                this.jatekviewModel.Laszti.PowerUpGone();
                this.jatekviewModel.PowerUpActive = false;
            }
        }       
    }
}
